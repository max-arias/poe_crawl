'use strict';

angular.module('poeCrawlApp')
  .service('search', ['$http', function ($http) {
    
    this.find = function(data){
    	
    	return $http.post('/api/find', data);
    }

  	return this;

  }]);
