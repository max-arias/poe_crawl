'use strict';

var express = require('express');
var controller = require('./find.controller');

var router = express.Router();

router.post('/', controller.find);

module.exports = router;
