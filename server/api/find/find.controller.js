'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var fs = require('fs');
var request = require('request');
var Q = require("q");
var util = require('util');

var ItemModel = require('../crawl/crawl.item.model');

exports.find = function(req, res) {

	var searchQuery = {};

	if (req.body.name) {
	    searchQuery.name = req.body.name;
	}


	ItemModel
		.find(searchQuery)
		.limit(100)
		.exec(function (err, docs) {
		    if(err) handleError(res, err);

			res.send(200, docs);
		    
		});



};

function handleError(res, err) {
	return res.send(500, err);
}
