'use strict';

var currencyAbbreviations = [
  "chrom",
  "alt",
  "jewel",
  "chance",
  "chisel",
  "fuse",
  "alch",
  "scour",
  "blessed",
  "chaos",
  "regret",
  "regal",
  "gcp",
  "divine",
  "exa",
  "vaal"
];

var buyOut = "~b/o";