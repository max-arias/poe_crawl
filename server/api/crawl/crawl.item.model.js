'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

//--- Item Schema ---
var itemSchema = new Schema({
	verified: Boolean,
	w: Number,
	h: Number,
	icon: String,
	support: Boolean,
	league: String,
	sockets: [],
	name: String,
	typeLine: String,
	identified: Boolean,
	corrupted: Boolean,
	/*requirements  : [],*/
	implicitMods: [],
	explicitMods: [],
	flavourText: [],
	frameType: Number,
	socketedItems: [],
	threadId: Number,
	buyOut: {},
	crawlTime: Date
});

//--- Item Model
module.exports = mongoose.model('Item', itemSchema);
