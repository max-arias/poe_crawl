'use strict';

var _ = require('lodash');
var cheerio = require('cheerio');
var fs = require('fs');
var request = require('request');
var Q = require("q");

var ItemModel = require('./crawl.item.model');
var ItemModModel = require('./crawl.itemMod.model');

var ShopController = require('./crawl.controller.shop');
var ThreadController = require('./crawl.controller.thread');


var mainPage = 'http://www.pathofexile.com/';
var standardTradingForum = 'http://www.pathofexile.com/forum/view-forum/standard-trading-shops';

var shopFile = __dirname + '/../../mock/shop.txt';
var threadFile = __dirname + '/../../mock/shopthread.txt';

var buyOut = "~b/o";

var baseRequest = request.defaults({
  jar            : true,
  followRedirect : false,
  headers        : {
    'User-Agent' : 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36'
  }
});

// Get list of crawls
exports.index = function(req, res) {

	req.accepts('json');

	var startExec = Date.now();

	//TODO: Fetch last crawled threads (last 2 mins)
	//Make sure that the crawled thread has been updated after the last crawl or it isn't in the crawled list
	
	fetchShopPage() // Fetch shop forum
		.then(function(shopHtml) {
			return parseShopPage(shopHtml);
		}) //parse shop, get shop urls
		.then(function(shopThreadUrls) {
			
			logData(JSON.stringify(shopThreadUrls));

			return parseThreadUrls(shopThreadUrls);
		}) //fetch shops
		.then(function(threadsHtml) {
			
			logData("Threads to parse: " + threadsHtml.length);
			
			return parseThreadHtmls(threadsHtml);
		}) //parse shops, return shop items
		.then(function(parsedShopItems) {
			return createItemMods(parsedShopItems);
		}) //create item mods, create implicits/explicits
		.then(function() {

			var endExec = Date.now();
			var totalTime = (endExec - startExec) / 1000;

			res.send(200, "Crawl finished in : " + totalTime);
		}) //Finished executing
		.fail(function(err) {
			handleError(res, err);
		}); //error

};

var fetchShopPage = function() {

	var deferred = Q.defer();

	/*fs.readFile(shopFile, 'utf8', function(err, data) {

		if (err) deferred.reject(err);

		deferred.resolve(data);
	});*/


	baseRequest({
		uri            : standardTradingForum,
		method         : "GET"
	}, function(err, response, body) {
		if(err) deferred.reject(err);

		//logData(body);

		deferred.resolve(body);
	});

	return deferred.promise;
};

var fetchShopThread = function(url) {

	var deferred = Q.defer();

	/*fs.readFile(threadFile, 'utf8', function(err, data) {

		if (err) deferred.reject(err);

		deferred.resolve(data);
	});*/

	baseRequest({
		uri            : url,
		method         : "GET"
	}, function(err, response, body) {
		if(err) deferred.reject(err);

		//logData(body);

		deferred.resolve(body);
	});

	return deferred.promise;
};

var parseThreadUrls = function(urls) {

	var deferred = Q.defer();

	var threadPromises = [];
	for (var i = 0; i < urls.length; i++) {
		var threadPromise = fetchShopThread(urls[i]);
		threadPromises.push(threadPromise);
	}

	return Q.all(threadPromises);
};

var parseThreadHtmls = function(threadsHtml) {

	var deferred = Q.defer();

	//TODO: Deferre these?

	//testing-->
	/*var parsedThreadTest = parseThreadPage(threadsHtml[0]);
	return parsedThreadTest;*/

	var parsedShopItems = [];
	//Loop array of thread htmls, parse them and save to DB
	for (var i = 0; i < threadsHtml.length; i++) {
		parsedShopItems.push(parseThreadPage(threadsHtml[i]));
	}

	deferred.resolve(parsedShopItems);

	return deferred.promise;

}

var parseShopPage = function(html, type) {

	var numStickies = type || 2;
	var $ = cheerio.load(html);

	var shopTable = $('#view_forum_table');
	var shopThreads = shopTable.find('tr td.thread .thread_title .title a');
	var shopThreadUrls = [];

	shopThreads.each(function() {
		shopThreadUrls.push(mainPage + $(this).attr('href'));
	});

	//Remove stickied
	shopThreadUrls.splice(0, numStickies);

	return shopThreadUrls;

};

var createItemMods = function(shopItems) {

	var deferred = Q.defer();

	if (!shopItems) {
		deferred.reject("No shop items!");
	}

	//First array is array of shop pages
	//second array is list of items in each shop page
	for (var i = 0; i < shopItems.length; i++) {
		for (var j = 0; j < shopItems[i].length; j++) {

			try {

				if (!shopItems[i][j].implicitMods || !shopItems[i][j].explicitMods) continue;

				createItemMod(shopItems[i][j].implicitMods);
				createItemMod(shopItems[i][j].explicitMods);

			} catch (e) {
				deferred.reject(e);
			}

		}

	}

	deferred.resolve("Items created");


	return deferred.promise;
}

var parseThreadPage = function(html) {

	var $ = cheerio.load(html);
	var shopExpression = new RegExp(/function\(R\) { \(new R\((.*)\)\).run\(\); \}\);/);
	var shopTest = html.match(shopExpression);

	if (!shopTest[1]) {
		logData("Error: couldn't parse shop thread HTML");
		return "Error: couldn't parse shop thread HTML";
	}

	var crawlTime = Date.now();

	var threadIdText = $('.mainButtons .button1.important').first().attr('href');
	var threadIdMatch = threadIdText.match(new RegExp(/([0-9])+/));
	var threadId;

	if (threadIdMatch[0]) {
		threadId = threadIdMatch[0];
	}

	//Find values for spoiler prices:
	var spoilerItemPrices = [];
	$('.spoiler .spoilerTitle span').each(function(i, elem) {
		var spoilerTitle = $(this).text();
		spoilerTitle.trim();
		spoilerTitle = spoilerTitle.replace('"', '');

		if (_.contains(spoilerTitle, buyOut)) {

			var spoilerItems = $(this).parents('.spoiler').find('.spoilerContent .itemContentLayout');

			if (spoilerItems.length) {
				spoilerItems.each(function() {
					var found_item = {
						id: $(this).attr('id'),
						value: spoilerTitle
					};
					spoilerItemPrices.push(found_item);
				});
			}

		}
	});

	if(spoilerItemPrices){
		logData("spoilerItemPrices " + threadId + " : " + JSON.stringify(spoilerItemPrices));
	}

	//Find values for inline prices:
	var inlineItemPrices = [];
	$('.content-container').find('.itemFragment').parent().contents().filter(function() {

		if (this.type === 'text') { //Find text nodes

			var innerText = this.data;
			innerText = innerText.trim();
			innerText = innerText.replace('"', '');

			if (innerText && _.contains(innerText, buyOut)) { //If text has buy out tag, it's a price tag

				var inlineItemId = $(this).prevAll('.itemContentLayout').first().attr('id');
				var found_item_data = {
					id: inlineItemId,
					value: innerText
				};

				inlineItemPrices.push(found_item_data);
			}

		}
	});

	if(inlineItemPrices){
		logData("inlineItemPrices " + threadId + " : " + JSON.stringify(inlineItemPrices));
	}

	//Iterate thread data
	if (shopTest[1]) {

		var parsedData = JSON.parse(shopTest[1]); //Convert string to object

		var itemsOut = [];

		for (var i = 0; i < parsedData.length; i++) {

			try {
				var itemInfo = parsedData[i][1];

				itemInfo.buyOut = null;

				//Save current crawl time
				itemInfo.crawlTime = crawlTime

				//Set Thread id into the item for later filtering
				if (threadId) itemInfo.threadId = threadId;

				//Set name for items without "name" (ie. Gems)
				if (!itemInfo.name && itemInfo.typeLine) itemInfo.name = itemInfo.typeLine;

				if(itemInfo.name){
					itemInfo.search_name = itemInfo.name.toLowerCase().trim();
				}

				var implicitMods = itemInfo.implicitMods;
				var explicitMods = itemInfo.explicitMods;
				var craftedMods  = itemInfo.craftedMods;

				//Parse implicits and explicits so we can use them later
				//"displayText" should be saved to the DB so we can show it the FE dropdown(eg. #% regen)

				if (implicitMods) {
					var implicitTmp = parseItemMods(implicitMods);
					itemInfo.implicitMods = implicitTmp;
				}

				if (explicitMods) {
					var explicitTmp = parseItemMods(explicitMods);
					itemInfo.explicitMods = explicitTmp;
				}

				if (craftedMods) {
					var craftedTmp = parseItemMods(craftedMods);
					itemInfo.craftedMods = craftedTmp;
				}

				//TODO: Optimize this:

				//Check if this item has a buyout, array iteration should match item fragment id
				var foundBuyOut = _.find(inlineItemPrices, function(inlineItemPrice) {
					return inlineItemPrice.id === "item-fragment-" + i;
				});

				if(!foundBuyOut){
					foundBuyOut = _.find(spoilerItemPrices, function(spoilerItemPrice) {
						return spoilerItemPrice.id === "item-fragment-" + i;
					});
				}

				//If found, parse it so we can make use of it later
				if (foundBuyOut) {
					itemInfo.buyOut = parseItemBuyout(foundBuyOut);
				}

				ItemModel.create(itemInfo);

				itemsOut.push(itemInfo);

			} catch (e) {
				logData(e);
				console.log(e);
			}

		}

		return itemsOut;
	}

};

var parseItemBuyout = function(bo) {
	var numberExpression = new RegExp(/([0-9](-[0-9])?)+/);

	//TODO: Parse things like: "~c/o 118 ex ~b/o 120 ex"

	var currencyAbbreviations = [
		"chrom",
		"alt",
		"jewel",
		"chance",
		"chisel",
		"fuse",
		"alch",
		"scour",
		"blessed",
		"chaos",
		"regret",
		"regal",
		"gcp",
		"divine",
		"exa",
		"mirror",
		"eternal",
		"vaal"
	];
	var currencyExpression = new RegExp("(" + currencyAbbreviations.join("|") + ")+");

	var buyOutValue = bo.value;

	var numMatch = buyOutValue.match(numberExpression);
	var currencyMatch = buyOutValue.match(currencyExpression);

	var buyOut = {
		displayText: buyOutValue,
		numberValue: null,
		currencyType: null
	};

	if (numMatch && numMatch[0]) {
		buyOut.numberValue = numMatch[0];
	}

	if (currencyMatch && currencyMatch[0]) {
		buyOut.currencyType = currencyMatch[0];
	}

	return buyOut;
}

var parseItemMods = function(modArray) {

	var numberExpression = new RegExp(/([0-9](-[0-9])?)+/);
	var modTmp = [];

	for (var i = 0; i < modArray.length; i++) {
		var modItem = modArray[i];

		var modMatch = modItem.match(numberExpression);

		var modItem = {
			displayText: modItem,
			numberValue: null
		};

		if (modMatch && modMatch[0]) {
			modItem.numberValue = modMatch[0];
			modItem.displayText = modItem.displayText.replace(numberExpression, "#");
		}

		modTmp.push(modItem);
	}

	return modTmp;
}

var createItemMod = function(mod) {

	for (var i = 0; i < mod.length; i++) {
		ItemModModel.create({
			name: mod[i].displayText
		});
	}
}

var handleError = function(res, err) {
	return res.send(500, err);
}

var logData = function(data, log){

	log = log || 'log.txt';

	var log_dir = __dirname + '/../../log/';
	
	fs.appendFile(log_dir + log, data + "\r\n\r\n-------------------\r\n\r\n\r\n", function (err) {
	  //if (err) throw err;
	  console.log('Data logged');
	});


}


/*

  EXAMPLE:

  ~b\/o [0-9]+ chaos


var currencyNames = [
  {"Chromatic Orb"         : "chrom"},
  {"Orb of Alteration"     : "alt"},
  {"Jeweller's Orb"        : "jewel"},
  {"Orb of Chance"         : "chance"},
  {"Cartographer's Chisel" : "chisel"},
  {"Orb of Fusing"         : "fuse"},
  {"Orb of Alchemy"        : "alch"},
  {"Orb of Scouring"       : "scour"},
  {"Blessed Orb"           : "blessed"},
  {"Chaos Orb"             : "chaos"},
  {"Orb of Regret"         : "regret"},
  {"Regal Orb"             : "regal"},
  {"Gemcutter's Prism"     : "gcp"},
  {"Divine Orb"            : "divine"},
  {"Exalted Orb"           : "exa"},
  {"Vaal Orb"              : "vaal"}
];

*/
