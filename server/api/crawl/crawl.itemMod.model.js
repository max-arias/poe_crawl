'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

//--- Item Schema ---
var itemSchema = new Schema({
	name: String
});

//--- Item Model
module.exports = mongoose.model('ItemMod', itemSchema);
